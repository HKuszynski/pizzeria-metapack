﻿using System;
using System.Windows;
using System.Windows.Controls;
using Pizzeria.Configuration;
using Pizzeria.Logs;

namespace Pizzeria.OrderMenu
{
    /// <summary>
    /// Logika interakcji dla klasy DishSelectWindow.xaml
    /// </summary>
    public partial class DishSelectWindow : Window
    {
        private string _dishName;
        public DishSelectWindow()
        {
            try
            {
                InitializeComponent();
                menuimageDish.ImageSource = MenuParameters.imageDish;
                this.Background = menuimageDish;
                comboboxDish.Items.Add("Fried Porkchop with Fries");
                comboboxDish.Items.Add("Fish and Chips");
                comboboxDish.Items.Add("Hungarian Hashbrown");
                LocalParameters.RestoreParameters();
                textboxSumVal.Text = Convert.ToString(LocalParameters.sumValue);
                chkboxSalad.Visibility = Visibility.Hidden;
                chkboxSauce.Visibility = Visibility.Hidden;
            }
            catch(Exception e)
            {
                MessageBox.Show(e.Message);
                LogWriter.LogWrite(e.ToString());
            }
        }
        private void comboboxDish_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            _dishName = Convert.ToString(comboboxDish.SelectedItem);
            try
            {
                switch (_dishName)
                {
                    case ("Fried Porkchop with Fries"):
                        textboxAmount.Text = Convert.ToString(1);
                        LocalParameters.amount = Convert.ToInt32(textboxAmount.Text);
                        DishMaker.MakeDish(comboboxDish.SelectedIndex);
                        textboxDishVal.Text = Convert.ToString(LocalParameters.dishValue);
                        textboxSumVal.Text = Convert.ToString(LocalParameters.sumValue + LocalParameters.dishValue);
                        chkboxSalad.Visibility = Visibility.Visible;
                        chkboxSauce.Visibility = Visibility.Visible;
                        break;
                    case ("Fish and Chips"):
                        goto case "Fried Porkchop with Fries";
                    case ("Hungarian Hashbrown"):
                        goto case "Fried Porkchop with Fries";
                }
            }
            catch (Exception f)
            {
                MessageBox.Show(f.Message);
                LogWriter.LogWrite(f.ToString());
            }
        }
        private void SaladBar_Checked(object sender, RoutedEventArgs e)
        {
            try
            {
                DishMaker.AddSalad(comboboxDish.SelectedIndex);
                textboxDishVal.Text = Convert.ToString(LocalParameters.dishValue);
                textboxSumVal.Text = Convert.ToString(LocalParameters.sumValue + LocalParameters.dishValue);
                LocalParameters.dishAddonNum++;
            }
            catch (Exception g)
            {
                MessageBox.Show(g.Message);
                LogWriter.LogWrite(g.ToString());
            }
        }
        private void AdditionalSauce_Checked(object sender, RoutedEventArgs e)
        {
            try
            {
                DishMaker.AddSauce(comboboxDish.SelectedIndex);
                textboxDishVal.Text = Convert.ToString(LocalParameters.dishValue);
                textboxSumVal.Text = Convert.ToString(LocalParameters.sumValue + LocalParameters.dishValue);
                LocalParameters.dishAddonNum++;
            }
            catch (Exception h)
            {
                MessageBox.Show(h.Message);
                LogWriter.LogWrite(h.ToString());
            }
        }
        private void SaladBar_UnChecked(object sender, RoutedEventArgs e)
        {
            try
            {
                DishMaker.RemoveSalad(comboboxDish.SelectedIndex);
                textboxDishVal.Text = Convert.ToString(LocalParameters.dishValue);
                textboxSumVal.Text = Convert.ToString(LocalParameters.sumValue + LocalParameters.dishValue);
                LocalParameters.dishAddonNum--;
            }
            catch (Exception i)
            {
                MessageBox.Show(i.Message);
                LogWriter.LogWrite(i.ToString());
            }
        }
        private void AdditionalSauce_UnChecked(object sender, RoutedEventArgs e)
        {
            try
            {
                DishMaker.RemoveSauce(comboboxDish.SelectedIndex);
                textboxDishVal.Text = Convert.ToString(LocalParameters.dishValue);
                textboxSumVal.Text = Convert.ToString(LocalParameters.sumValue + LocalParameters.dishValue);
                LocalParameters.dishAddonNum--;
            }
            catch (Exception j)
            {
                MessageBox.Show(j.Message);
                LogWriter.LogWrite(j.ToString());
            }
        }
        private void TextboxAmount_TextChanged(object sender, TextChangedEventArgs e)
        {
            try
            {
                if (textboxAmount.Text == string.Empty)
                {
                    textboxAmount.Text = Convert.ToString(1);
                    LocalParameters.amount = Convert.ToInt32(textboxAmount.Text);
                    DishMaker.MakeDish(comboboxDish.SelectedIndex);
                    textboxDishVal.Text = Convert.ToString(LocalParameters.dishValue);
                    textboxSumVal.Text = Convert.ToString(LocalParameters.sumValue + LocalParameters.dishValue);
                }
                else if (textboxAmount.Text != string.Empty)
                {
                    LocalParameters.amount = Convert.ToInt32(textboxAmount.Text);
                    DishMaker.MakeDish(comboboxDish.SelectedIndex);
                    textboxDishVal.Text = Convert.ToString(LocalParameters.dishValue);
                    textboxSumVal.Text = Convert.ToString(LocalParameters.sumValue + LocalParameters.dishValue);
                }
            }
            catch (Exception m)
            {
                MessageBox.Show("Only numeric characters are allowed! Use numpad to input :)");
                LogWriter.LogWrite(m.ToString());
            }
        }
        private void AddToOrder_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (comboboxDish.SelectedIndex >= 0)
                {
                    MenuParameters.orderTable.Rows.Add(_dishName, LocalParameters.amount, LocalParameters.dishAddonNum, LocalParameters.dishValue);
                    LocalParameters.sumValue += LocalParameters.dishValue;
                    textboxSumVal.Text = Convert.ToString(LocalParameters.sumValue);
                    LogWriter.LogWrite($"Added to receipt {LocalParameters.amount} dish/es of value {LocalParameters.dishValue} PLN. Actual sum of ordered items is: {LocalParameters.sumValue}");
                    this.Close();
                }
                else
                {
                    MessageBox.Show("You must select an option from combobox to add order to receipt");
                }
            }
            catch (Exception f)
            {
                MessageBox.Show(f.Message);
                LogWriter.LogWrite(f.ToString());
            }
        }
    }
}
