﻿using System;
using System.Windows;
using System.Windows.Controls;
using Pizzeria.Configuration;
using Pizzeria.Logs;

namespace Pizzeria.OrderMenu
{
    /// <summary>
    /// Logika interakcji dla klasy DrinkSelectWindow.xaml
    /// </summary>
    public partial class DrinkSelectWindow : Window
    {
        private string _drinkName;
        public DrinkSelectWindow()
        {
            try
            {
                InitializeComponent();
                menuimageDrink.ImageSource = MenuParameters.imageDrinks;
                this.Background = menuimageDrink;
                comboboxDrink.Items.Add("Cola");
                comboboxDrink.Items.Add("Tea");
                comboboxDrink.Items.Add("Coffee");
                LocalParameters.RestoreParameters();
                textboxSumVal.Text = Convert.ToString(LocalParameters.sumValue);
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message);
                LogWriter.LogWrite(e.ToString());
            }
        }

        private void comboboxDrink_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            _drinkName = Convert.ToString(comboboxDrink.SelectedItem);
            try
            {
                switch (_drinkName)
                {
                    case ("Cola"):
                        if (textboxAmount.Text == string.Empty)
                        textboxAmount.Text = Convert.ToString(1);
                        LocalParameters.amount = Convert.ToInt32(textboxAmount.Text);
                        DrinkMaker.MakeDrink(comboboxDrink.SelectedIndex);
                        textboxDrinkVal.Text = Convert.ToString(LocalParameters.drinkValue);
                        textboxSumVal.Text = Convert.ToString(LocalParameters.drinkValue + LocalParameters.sumValue);
                        break;
                    case ("Tea"):
                        goto case "Cola";
                    case ("Coffee"):
                        goto case "Cola";
                }
            }
            catch (Exception f)
            {
                MessageBox.Show(f.Message);
                LogWriter.LogWrite(f.ToString());
            }
        }
        private void TextboxAmount_TextChanged(object sender, TextChangedEventArgs e)
        {
            try
            {
                if (textboxAmount.Text == string.Empty)
                {
                    textboxAmount.Text = Convert.ToString(1);
                    LocalParameters.amount = Convert.ToInt32(textboxAmount.Text);
                    DrinkMaker.MakeDrink(comboboxDrink.SelectedIndex);
                    textboxDrinkVal.Text = Convert.ToString(LocalParameters.drinkValue);
                    textboxSumVal.Text = Convert.ToString(LocalParameters.drinkValue + LocalParameters.sumValue);
                }
                else if (textboxAmount.Text != string.Empty)
                {
                    LocalParameters.amount = Convert.ToInt32(textboxAmount.Text);
                    DrinkMaker.MakeDrink(comboboxDrink.SelectedIndex);
                    textboxDrinkVal.Text = Convert.ToString(LocalParameters.drinkValue);
                    textboxSumVal.Text = Convert.ToString(LocalParameters.drinkValue + LocalParameters.sumValue);
                }
            }
            catch (Exception g)
            {
                MessageBox.Show("Only numeric characters are allowed! Use numpad to input :)");
                LogWriter.LogWrite(g.ToString());
            }
        }
        private void AddToOrder_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (comboboxDrink.SelectedIndex >= 0)
                {
                    MenuParameters.orderTable.Rows.Add(_drinkName, LocalParameters.amount, string.Empty, LocalParameters.drinkValue);
                    LocalParameters.sumValue += LocalParameters.drinkValue;
                    textboxSumVal.Text = Convert.ToString(LocalParameters.sumValue);
                    this.Close();
                    LogWriter.LogWrite($"Added to receipt {LocalParameters.amount} drink/s of value {LocalParameters.drinkValue} PLN. Actual sum of ordered items is {LocalParameters.sumValue}");
                }
                else
                {
                    MessageBox.Show("You must select an option from combobox to add order to receipt");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                LogWriter.LogWrite(ex.ToString());
            }
        }
    }
}
