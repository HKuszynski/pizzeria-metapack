﻿using Pizzeria.Configuration;

namespace Pizzeria.OrderMenu
{
    public static class PizzaMaker
    {
        public static void MakePizza(int option)
        {
            if (option == 0)
            {
                LocalParameters.pizzaValue = LocalParameters.margheritaVal * LocalParameters.amount;
            }
            else if (option == 1)
            {
                LocalParameters.pizzaValue = LocalParameters.vegetarianaVal * LocalParameters.amount;
            }
            else if (option == 2)
            {
                LocalParameters.pizzaValue = LocalParameters.toscaVal * LocalParameters.amount;
            }
            else if (option == 3)
            {
                LocalParameters.pizzaValue = LocalParameters.veneciaVal * LocalParameters.amount;
            }
        }
        public static void AddCheese(int option)
        {
            LocalParameters.margheritaVal += LocalParameters.pizzaAddonCheese;
            LocalParameters.vegetarianaVal += LocalParameters.pizzaAddonCheese;
            LocalParameters.toscaVal += LocalParameters.pizzaAddonCheese;
            LocalParameters.veneciaVal += LocalParameters.pizzaAddonCheese;
            MakePizza(option);
        }
        public static void AddHam(int option)
        {
            LocalParameters.margheritaVal += LocalParameters.pizzaAddonHam;
            LocalParameters.vegetarianaVal += LocalParameters.pizzaAddonHam;
            LocalParameters.toscaVal += LocalParameters.pizzaAddonHam;
            LocalParameters.veneciaVal += LocalParameters.pizzaAddonHam;
            MakePizza(option);
        }
        public static void AddSalami(int option)
        {
            LocalParameters.margheritaVal += LocalParameters.pizzaAddonSalami;
            LocalParameters.vegetarianaVal += LocalParameters.pizzaAddonSalami;
            LocalParameters.toscaVal += LocalParameters.pizzaAddonSalami;
            LocalParameters.veneciaVal += LocalParameters.pizzaAddonSalami;
            MakePizza(option);
        }
        public static void AddMushroom(int option)
        {
            LocalParameters.margheritaVal += LocalParameters.pizzaAddonMushroom;
            LocalParameters.vegetarianaVal += LocalParameters.pizzaAddonMushroom;
            LocalParameters.toscaVal += LocalParameters.pizzaAddonMushroom;
            LocalParameters.veneciaVal += LocalParameters.pizzaAddonMushroom;
            MakePizza(option);
        }
        public static void RemoveCheese(int option)
        {
            LocalParameters.margheritaVal -= LocalParameters.pizzaAddonCheese;
            LocalParameters.vegetarianaVal -= LocalParameters.pizzaAddonCheese;
            LocalParameters.toscaVal -= LocalParameters.pizzaAddonCheese;
            LocalParameters.veneciaVal -= LocalParameters.pizzaAddonCheese;
            MakePizza(option);
        }
        public static void RemoveHam(int option)
        {
            LocalParameters.margheritaVal -= LocalParameters.pizzaAddonHam;
            LocalParameters.vegetarianaVal -= LocalParameters.pizzaAddonHam;
            LocalParameters.toscaVal -= LocalParameters.pizzaAddonHam;
            LocalParameters.veneciaVal -= LocalParameters.pizzaAddonHam;
            MakePizza(option);
        }
        public static void RemoveSalami(int option)
        {
            LocalParameters.margheritaVal -= LocalParameters.pizzaAddonSalami;
            LocalParameters.vegetarianaVal -= LocalParameters.pizzaAddonSalami;
            LocalParameters.toscaVal -= LocalParameters.pizzaAddonSalami;
            LocalParameters.veneciaVal -= LocalParameters.pizzaAddonSalami;
            MakePizza(option);
        }
        public static void RemoveMushroom(int option)
        {
            LocalParameters.margheritaVal -= LocalParameters.pizzaAddonCheese;
            LocalParameters.vegetarianaVal -= LocalParameters.pizzaAddonCheese;
            LocalParameters.toscaVal -= LocalParameters.pizzaAddonCheese;
            LocalParameters.veneciaVal -= LocalParameters.pizzaAddonCheese;
            MakePizza(option);
        }
    }
}
