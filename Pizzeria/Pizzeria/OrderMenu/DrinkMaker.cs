﻿using Pizzeria.Configuration;

namespace Pizzeria.OrderMenu
{
    public static class DrinkMaker
    {
        public static void MakeDrink(int option)
        {
            if (option == 0)
            {
                LocalParameters.drinkValue = LocalParameters.cola * LocalParameters.amount;
            }
            else if (option == 1)
            {
                LocalParameters.drinkValue = LocalParameters.tea * LocalParameters.amount;
            }
            else if (option == 2)
            {
                LocalParameters.drinkValue = LocalParameters.coffee * LocalParameters.amount;
            }
        }
    }
}
