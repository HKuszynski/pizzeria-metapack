﻿using System;
using System.Windows;
using System.Windows.Controls;
using Pizzeria.Configuration;
using Pizzeria.Logs;

namespace Pizzeria.OrderMenu
{
    /// <summary>
    /// Logika interakcji dla klasy PizzaSelectWindow.xaml
    /// </summary>
    public partial class PizzaSelectWindow : Window
    {
        private string _pizzaName;
        public PizzaSelectWindow()
        {
            try
            {
                InitializeComponent();
                pizzaImage.ImageSource = MenuParameters.imagePizza;
                this.Background = pizzaImage;
                comboboxPizza.Items.Add("Margherita");
                comboboxPizza.Items.Add("Vegetariana");
                comboboxPizza.Items.Add("Tosca");
                comboboxPizza.Items.Add("Venecia");
                LocalParameters.RestoreParameters();
                textboxSumVal.Text = Convert.ToString(LocalParameters.sumValue);
                chkboxCheese.Visibility = Visibility.Hidden;
                chkboxHam.Visibility = Visibility.Hidden;
                chkboxSalami.Visibility = Visibility.Hidden;
                chkboxMushroom.Visibility = Visibility.Hidden;
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message);
                LogWriter.LogWrite(e.ToString());
            }
        }
        private void ComboboxPizza_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            _pizzaName = Convert.ToString(comboboxPizza.SelectedItem);
            try
            {
                switch (_pizzaName)
                {
                    case "Margherita":
                        textboxAmount.Text = Convert.ToString(1);
                        LocalParameters.amount = Convert.ToInt32(textboxAmount.Text);
                        PizzaMaker.MakePizza(comboboxPizza.SelectedIndex);
                        textboxPizzaVal.Text = Convert.ToString(LocalParameters.pizzaValue);
                        textboxSumVal.Text = Convert.ToString(LocalParameters.sumValue + LocalParameters.pizzaValue);
                        chkboxCheese.Visibility = Visibility.Visible;
                        chkboxHam.Visibility = Visibility.Visible;
                        chkboxSalami.Visibility = Visibility.Visible;
                        chkboxMushroom.Visibility = Visibility.Visible;
                        break;
                    case "Vegetariana":
                        goto case "Margherita";
                    case "Tosca":
                        goto case "Margherita";
                    case "Venecia":
                        goto case "Margherita";
                }
            }
            catch (Exception f)
            {
                MessageBox.Show(f.Message);
                LogWriter.LogWrite(f.ToString());
            }
        }
        private void DoubleCheese_Checked(object sender, RoutedEventArgs e)
        {
            try
            {
                PizzaMaker.AddCheese(comboboxPizza.SelectedIndex);
                textboxPizzaVal.Text = Convert.ToString(LocalParameters.pizzaValue);
                textboxSumVal.Text = Convert.ToString(LocalParameters.pizzaValue + LocalParameters.sumValue);
                LocalParameters.pizzaAddonNum++;
                LogWriter.LogWrite("Added double cheese");
            }
            catch (Exception g)
            {
                MessageBox.Show(g.Message);
                LogWriter.LogWrite(g.ToString());
            }
        }
        private void Salami_Checked(object sender, RoutedEventArgs e)
        {
            try
            {
                PizzaMaker.AddSalami(comboboxPizza.SelectedIndex);
                textboxPizzaVal.Text = Convert.ToString(LocalParameters.pizzaValue);
                textboxSumVal.Text = Convert.ToString(LocalParameters.pizzaValue + LocalParameters.sumValue);
                LocalParameters.pizzaAddonNum++;
                LogWriter.LogWrite("Added addidional salami");
            }
            catch (Exception h)
            {
                MessageBox.Show(h.Message);
                LogWriter.LogWrite(h.ToString());
            }
        }
        private void Ham_Checked(object sender, RoutedEventArgs e)
        {
            try
            {
                PizzaMaker.AddHam(comboboxPizza.SelectedIndex);
                textboxPizzaVal.Text = Convert.ToString(LocalParameters.pizzaValue);
                textboxSumVal.Text = Convert.ToString(LocalParameters.pizzaValue + LocalParameters.sumValue);
                LocalParameters.pizzaAddonNum++;
                LogWriter.LogWrite("Added addidional ham");
            }
            catch (Exception i)
            {
                MessageBox.Show(i.Message);
                LogWriter.LogWrite(i.ToString());
            }
        }
        private void Mushroom_Checked(object sender, RoutedEventArgs e)
        {
            try
            {
                PizzaMaker.AddMushroom(comboboxPizza.SelectedIndex);
                textboxPizzaVal.Text = Convert.ToString(LocalParameters.pizzaValue);
                textboxSumVal.Text = Convert.ToString(LocalParameters.pizzaValue + LocalParameters.sumValue);
                LocalParameters.pizzaAddonNum++;
                LogWriter.LogWrite("Added addidional mushrooms");
            }
            catch (Exception j)
            {
                MessageBox.Show(j.Message);
                LogWriter.LogWrite(j.ToString());
            }
        }
        private void DoubleCheese_UnChecked(object sender, RoutedEventArgs e)
        {
            try
            {
                PizzaMaker.RemoveCheese(comboboxPizza.SelectedIndex);
                textboxPizzaVal.Text = Convert.ToString(LocalParameters.pizzaValue);
                textboxSumVal.Text = Convert.ToString(LocalParameters.pizzaValue + LocalParameters.sumValue);
                LocalParameters.pizzaAddonNum--;
                LogWriter.LogWrite("Removed additional cheese..");
            }
            catch (Exception k)
            {
                MessageBox.Show(k.Message);
                LogWriter.LogWrite(k.ToString());
            }
        }
        private void Salami_UnChecked(object sender, RoutedEventArgs e)
        {
            try
            {
                PizzaMaker.RemoveSalami(comboboxPizza.SelectedIndex);
                textboxPizzaVal.Text = Convert.ToString(LocalParameters.pizzaValue);
                textboxSumVal.Text = Convert.ToString(LocalParameters.pizzaValue + LocalParameters.sumValue);
                LocalParameters.pizzaAddonNum--;
                LogWriter.LogWrite("Removed additional salami..");
            }
            catch (Exception l)
            {
                MessageBox.Show(l.Message);
                LogWriter.LogWrite(l.ToString());
            }
        }
        private void Ham_UnChecked(object sender, RoutedEventArgs e)
        {
            try
            {
                PizzaMaker.RemoveHam(comboboxPizza.SelectedIndex);
                textboxPizzaVal.Text = Convert.ToString(LocalParameters.pizzaValue);
                textboxSumVal.Text = Convert.ToString(LocalParameters.pizzaValue + LocalParameters.sumValue);
                LocalParameters.pizzaAddonNum--;
                LogWriter.LogWrite("Removed additional ham..");
            }
            catch (Exception m)
            {
                MessageBox.Show(m.Message);
                LogWriter.LogWrite(m.ToString());
            }
        }
        private void Mushroom_UnChecked(object sender, RoutedEventArgs e)
        {
            try
            {
                PizzaMaker.RemoveMushroom(comboboxPizza.SelectedIndex);
                textboxPizzaVal.Text = Convert.ToString(LocalParameters.pizzaValue);
                textboxSumVal.Text = Convert.ToString(LocalParameters.pizzaValue + LocalParameters.sumValue);
                LocalParameters.pizzaAddonNum--;
                LogWriter.LogWrite("Removed additional mushrooms..");
            }
            catch (Exception n)
            {
                MessageBox.Show(n.Message);
                LogWriter.LogWrite(n.ToString());
            }
        }
        private void TextboxAmount_TextChanged(object sender, TextChangedEventArgs e)
        {
            try
            {
                if (textboxAmount.Text == string.Empty)
                {
                    textboxAmount.Text = Convert.ToString(1);
                    LocalParameters.amount = Convert.ToInt32(textboxAmount.Text);
                    PizzaMaker.MakePizza(comboboxPizza.SelectedIndex);
                    textboxPizzaVal.Text = Convert.ToString(LocalParameters.pizzaValue);
                    textboxSumVal.Text = Convert.ToString(LocalParameters.pizzaValue + LocalParameters.sumValue);
                }
                else if (textboxAmount.Text != string.Empty)
                {
                    LocalParameters.amount = Convert.ToInt32(textboxAmount.Text);
                    PizzaMaker.MakePizza(comboboxPizza.SelectedIndex);
                    textboxPizzaVal.Text = Convert.ToString(LocalParameters.pizzaValue);
                    textboxSumVal.Text = Convert.ToString(LocalParameters.pizzaValue + LocalParameters.sumValue);
                }
            }
            catch (Exception o)
            {
                MessageBox.Show("Only numeric characters are allowed! Use numpad to input :)");
                LogWriter.LogWrite(o.ToString());
            }
        }
        private void AddToOrder_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (comboboxPizza.SelectedIndex >= 0)
                {
                    MenuParameters.orderTable.Rows.Add(_pizzaName, LocalParameters.amount, LocalParameters.pizzaAddonNum, LocalParameters.pizzaValue);
                    LocalParameters.sumValue += LocalParameters.pizzaValue;
                    textboxSumVal.Text = Convert.ToString(LocalParameters.sumValue);
                    this.Close();
                    LogWriter.LogWrite($"Added to receipt {LocalParameters.amount} pizza/s of value {LocalParameters.pizzaValue} PLN. Actual sum of ordered items is: {LocalParameters.sumValue}");
                }
                else
                {
                    MessageBox.Show("You must select an option from combobox to add order to receipt");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                LogWriter.LogWrite(ex.ToString());
            }
        }
    }
}
