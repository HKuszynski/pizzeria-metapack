﻿using Pizzeria.Configuration;

namespace Pizzeria.OrderMenu
{
    public static class DishMaker
    {
        public static void MakeDish(int option)
        {
            if (option == 0)
            {
                LocalParameters.dishValue = LocalParameters.porkchopWithFries * LocalParameters.amount;
            }
            else if (option == 1)
            {
                LocalParameters.dishValue = LocalParameters.fishAndChips * LocalParameters.amount;
            }
            else if (option == 2)
            {
                LocalParameters.dishValue = LocalParameters.hungaryHashbrown * LocalParameters.amount;
            }
        }
        public static void AddSauce(int option)
        {
            LocalParameters.porkchopWithFries += LocalParameters.dishAddonSauce;
            LocalParameters.fishAndChips += LocalParameters.dishAddonSauce;
            LocalParameters.hungaryHashbrown += LocalParameters.dishAddonSauce;
            MakeDish(option);
        }
        public static void AddSalad(int option)
        {
            LocalParameters.porkchopWithFries += LocalParameters.dishAddonSalad;
            LocalParameters.fishAndChips += LocalParameters.dishAddonSalad;
            LocalParameters.hungaryHashbrown += LocalParameters.dishAddonSalad;
            MakeDish(option);
        }
        public static void RemoveSauce(int option)
        {
            LocalParameters.porkchopWithFries -= LocalParameters.dishAddonSauce;
            LocalParameters.fishAndChips -= LocalParameters.dishAddonSauce;
            LocalParameters.hungaryHashbrown -= LocalParameters.dishAddonSauce;
            MakeDish(option);
        }
        public static void RemoveSalad(int option)
        {
            LocalParameters.porkchopWithFries -= LocalParameters.dishAddonSalad;
            LocalParameters.fishAndChips -= LocalParameters.dishAddonSalad;
            LocalParameters.hungaryHashbrown -= LocalParameters.dishAddonSalad;
            MakeDish(option);
        }
    }
}
