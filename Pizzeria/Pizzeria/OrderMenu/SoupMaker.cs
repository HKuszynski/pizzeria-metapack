﻿using Pizzeria.Configuration;

namespace Pizzeria.OrderMenu
{
    public static class SoupMaker
    {
        public static void MakeSoup(int option)
        {
            if (option == 0)
            {
                LocalParameters.soupValue = LocalParameters.tomatoSoup * LocalParameters.amount;
            }
            else if (option == 1)
            {
                LocalParameters.soupValue = LocalParameters.chickenSoup * LocalParameters.amount;
            }
        }
    }
}
