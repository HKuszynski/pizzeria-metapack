﻿using System;
using System.Windows;
using System.Windows.Controls;
using Pizzeria.Logs;
using Pizzeria.Configuration;

namespace Pizzeria.OrderMenu
{
    /// <summary>
    /// Logika interakcji dla klasy SoupSelectWindow.xaml
    /// </summary>
    public partial class SoupSelectWindow : Window
    {
        private string _soupName;
        public SoupSelectWindow()
        {
            try
            {
                InitializeComponent();
                soupImageMenu.ImageSource = MenuParameters.imageSoup;
                this.Background = soupImageMenu;
                comboboxSoup.Items.Add("Tomato Soup");
                comboboxSoup.Items.Add("Chicken Soup");
                LocalParameters.RestoreParameters();
                textboxSumVal.Text = Convert.ToString(LocalParameters.sumValue);
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message);
                LogWriter.LogWrite(e.ToString());
            }
        }

        private void comboboxSoup_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            _soupName = Convert.ToString(comboboxSoup.SelectedItem);
            try
            {
                switch (_soupName)
                {
                    case ("Tomato Soup"):
                        textboxAmount.Text = Convert.ToString(1);
                        LocalParameters.amount = Convert.ToInt32(textboxAmount.Text);
                        SoupMaker.MakeSoup(comboboxSoup.SelectedIndex);
                        textboxSoupVal.Text = Convert.ToString(LocalParameters.soupValue);
                        textboxSumVal.Text = Convert.ToString(LocalParameters.soupValue + LocalParameters.sumValue);
                        break;
                    case ("Chicken Soup"):
                        goto case "Tomato Soup";
                }
            }
            catch (Exception f)
            {
                MessageBox.Show(f.Message);
                LogWriter.LogWrite(f.ToString());
            }
        }
        private void TextboxAmount_TextChanged(object sender, TextChangedEventArgs e)
        {
            try
            {
                if (textboxAmount.Text == string.Empty)
                {
                    textboxAmount.Text = Convert.ToString(1);
                    LocalParameters.amount = Convert.ToInt32(textboxAmount.Text);
                    SoupMaker.MakeSoup(comboboxSoup.SelectedIndex);
                    textboxSoupVal.Text = Convert.ToString(LocalParameters.soupValue);
                    textboxSumVal.Text = Convert.ToString(LocalParameters.soupValue + LocalParameters.sumValue);
                }
                else if (textboxAmount.Text != string.Empty)
                {
                    LocalParameters.amount = Convert.ToInt32(textboxAmount.Text);
                    SoupMaker.MakeSoup(comboboxSoup.SelectedIndex);
                    textboxSoupVal.Text = Convert.ToString(LocalParameters.soupValue);
                    textboxSumVal.Text = Convert.ToString(LocalParameters.soupValue + LocalParameters.sumValue);
                }
            }
            catch (Exception g)
            {
                MessageBox.Show("Only numeric characters are allowed! Use numpad to input :)");
                LogWriter.LogWrite(g.ToString());
            }
        }
        private void AddToOrder_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (comboboxSoup.SelectedIndex >= 0)
                {
                    MenuParameters.orderTable.Rows.Add(_soupName, LocalParameters.amount, string.Empty, LocalParameters.soupValue);
                    LocalParameters.sumValue += LocalParameters.soupValue;
                    textboxSumVal.Text = Convert.ToString(LocalParameters.sumValue);
                    LogWriter.LogWrite($"Added to receipt {LocalParameters.amount} soup/s of value {LocalParameters.soupValue} PLN. Actual order value is {LocalParameters.sumValue}");
                    this.Close();
                }
                else
                {
                    MessageBox.Show("You must select an option from combobox to add order to receipt");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                LogWriter.LogWrite(ex.ToString());
            }
        }
    }
}
