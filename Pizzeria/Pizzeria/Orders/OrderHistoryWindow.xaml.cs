﻿using System;
using System.Windows;
using Pizzeria.Configuration;
using Pizzeria.Connection;
using Pizzeria.Logs;

namespace Pizzeria.Orders
{
    /// <summary>
    /// Logika interakcji dla klasy OrderHistoryWindow.xaml
    /// </summary>
    public partial class OrderHistoryWindow : Window
    {
        public OrderHistoryWindow()
        {
            try
            {
                InitializeComponent();
                LocalSQLConnection connection = new LocalSQLConnection();
                connection.InitializeDBConnection();
                gridHistoryOrder.ItemsSource = MenuParameters.orderHistoryTable.DefaultView;
                LogWriter.LogWrite("Order History Loaded successfully.");
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message);
                LogWriter.LogWrite(e.ToString());
            }
        }
        private void buttonClose_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
            gridHistoryOrder.ItemsSource = null;
            MenuParameters.orderHistoryTable.Clear();
        }
    }
}
