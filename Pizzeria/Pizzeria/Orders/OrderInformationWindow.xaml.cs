﻿using System;
using System.Windows;
using Pizzeria.Connection;
using Pizzeria.Configuration;
using Pizzeria.Logs;

namespace Pizzeria.Orders
{
    /// <summary>
    /// Logika interakcji dla klasy OrderInformationWindow.xaml
    /// </summary>
    public partial class OrderInformationWindow : Window
    {
        public OrderInformationWindow()
        {
            try
            {
                InitializeComponent();
                ordermenuImage.ImageSource = MenuParameters.imageReceipt;
                this.Background = ordermenuImage;
                textboxSum.Text = Convert.ToString(LocalParameters.sumValue);
                LogWriter.LogWrite("Loaded Additional information window");
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message);
                LogWriter.LogWrite(e.ToString());
            }
        }
        private void buttonPlaceOrder_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                LocalParameters.postAddress = textboxAddress.Text;
                LocalParameters.emailAddress = textboxEmail.Text;
                LocalParameters.addInformation = textboxInfo.Text;
                MailSender.MailSend();
                LocalSQLConnection connection = new LocalSQLConnection();
                connection.InsertDBHistoryRecord();
                MenuParameters.orderTable.Clear();
                LocalParameters.sumValue = 0;
                this.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                LogWriter.LogWrite(ex.ToString());
            }
        }
    }
}
