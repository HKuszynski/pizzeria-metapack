﻿using System;
using System.Windows;
using System.IO;
using Pizzeria.Configuration;

namespace Pizzeria.Logs
{
    public static class LogWriter
    {
        public static void LogWrite(string logMessage)
        {
            if (!Directory.Exists(LocalParameters.loggingPath))
            {
                Directory.CreateDirectory(LocalParameters.loggingPath);
            }
            if (!File.Exists(LocalParameters.loggingPath + DateTime.Now.ToShortDateString() + "-" + Environment.MachineName + ".txt"))
            {
                File.Create(LocalParameters.loggingPath + DateTime.Now.ToShortDateString() + "-" + Environment.MachineName + ".txt").Close();
            }
            try
            {
                using (var writer = File.AppendText(LocalParameters.loggingPath + DateTime.Now.ToShortDateString() + "-" + Environment.MachineName + ".txt"))
                {
                    AppendLog(logMessage, writer); // Adding new line log.
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        private static void AppendLog(string logMessage, TextWriter txtWriter)
        {
            try
            {
                txtWriter.Write("\n");
                txtWriter.WriteLine("{0} {1}:{2}", DateTime.Now.ToShortDateString(), DateTime.Now.ToLongTimeString(), logMessage);
                txtWriter.Close();
            }
            catch (Exception ex)
            {
                LogWrite(ex.ToString());
            }
        }
    }
}
