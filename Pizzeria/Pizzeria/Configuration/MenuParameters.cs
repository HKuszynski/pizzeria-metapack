﻿using System;
using System.Data;
using System.Windows.Media.Imaging;

namespace Pizzeria.Configuration
{
    internal static class MenuParameters
    {
        internal static DataTable orderTable = new DataTable();
        internal static DataTable orderHistoryTable = new DataTable();
        internal static BitmapImage imageMenu = new BitmapImage(new Uri(Environment.CurrentDirectory + @"\Images\image.jpg"));
        internal static BitmapImage imagePizza = new BitmapImage(new Uri(Environment.CurrentDirectory + @"\Images\pizza.jpg"));
        internal static BitmapImage imageDish = new BitmapImage(new Uri(Environment.CurrentDirectory + @"\Images\salad.jpg"));
        internal static BitmapImage imageSoup = new BitmapImage(new Uri(Environment.CurrentDirectory + @"\Images\soup2.jpg"));
        internal static BitmapImage imageDrinks = new BitmapImage(new Uri(Environment.CurrentDirectory + @"\Images\drinks.jpg"));
        internal static BitmapImage imageReceipt = new BitmapImage(new Uri(Environment.CurrentDirectory + @"\Images\receipt.jpg"));
    }
}
