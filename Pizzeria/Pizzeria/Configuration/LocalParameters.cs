﻿using System;

namespace Pizzeria.Configuration
{
    internal static class LocalParameters
    {
        internal static string localSqlPath = @"Data Source=(LocalDB)\MSSQLLocalDB;AttachDbFilename=" + Environment.CurrentDirectory + @"\Connection\OrderHistoryDB.mdf;Integrated Security = True";
        internal static string selectQuery = "SelectAll";
        internal static string addQuery = "HistoryNewRecord";
        internal static string loggingPath = Environment.CurrentDirectory + "\\Logs\\ProgramLogs\\";

        internal static decimal margheritaVal = 20.00m;
        internal static decimal vegetarianaVal = 22.00m;
        internal static decimal toscaVal = 25.00m;
        internal static decimal veneciaVal = toscaVal;
        internal static decimal pizzaAddonCheese = 2.00m;
        internal static decimal pizzaAddonSalami = pizzaAddonCheese;
        internal static decimal pizzaAddonHam = pizzaAddonCheese;
        internal static decimal pizzaAddonMushroom = pizzaAddonCheese;
        internal static decimal porkchopWithFries = 30.00m;
        internal static decimal fishAndChips = 28.00m;
        internal static decimal hungaryHashbrown = 27.00m;
        internal static decimal dishAddonSalad = 5.00m;
        internal static decimal dishAddonSauce = 6.00m;
        internal static decimal tomatoSoup = 12.00m;
        internal static decimal chickenSoup = 10.00m;
        internal static decimal cola = 5.00m;
        internal static decimal tea = cola;
        internal static decimal coffee = cola;
        internal static string emailAddress = string.Empty;
        internal static string postAddress = string.Empty;
        internal static string addInformation = string.Empty;
        internal static int amount { get; set; }
        internal static decimal dishValue { get; set; }
        internal static decimal pizzaValue { get; set; }
        internal static decimal drinkValue { get; set; }
        internal static decimal soupValue { get; set; }
        internal static int pizzaAddonNum { get; set; }
        internal static int dishAddonNum { get; set; }
        internal static decimal sumValue { get; set; }
        internal static void RestoreParameters()
        {
            margheritaVal = 20.00m;
            vegetarianaVal = 22.00m;
            toscaVal = 25.00m;
            veneciaVal = toscaVal;
            porkchopWithFries = 30.00m;
            fishAndChips = 28.00m;
            hungaryHashbrown = 27.00m;
            dishAddonNum = 0;
            pizzaAddonNum = 0;
            dishValue = 0;
            pizzaValue = 0;
            soupValue = 0;
            drinkValue = 0;
        }
    }
}
