﻿using System;
using System.Windows;
using System.Data;
using Pizzeria.Logs;
using Pizzeria.OrderMenu;
using Pizzeria.Orders;
using Pizzeria.Configuration;

namespace Pizzeria
{
    /// <summary>
    /// Logika interakcji dla klasy MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            try
            {
                InitializeComponent();
                mainMenuImage.ImageSource = MenuParameters.imageMenu;
                this.Background = mainMenuImage;
                MenuParameters.orderTable.Columns.Add("Dish Name", typeof(string));
                MenuParameters.orderTable.Columns.Add("Amount", typeof(int));
                MenuParameters.orderTable.Columns.Add("Num of add-ons", typeof(string));
                MenuParameters.orderTable.Columns.Add("Value", typeof(decimal));
                orderGrid.ItemsSource = MenuParameters.orderTable.DefaultView;
                textboxSum.Text = Convert.ToString(LocalParameters.sumValue) + " PLN";
            }
            catch (Exception f)
            {
                MessageBox.Show(f.Message);
                LogWriter.LogWrite(f.ToString());
            }

        }
        private void buttonPizza_Click(object sender, RoutedEventArgs e)
        {
            PizzaSelectWindow pizzaWin = new PizzaSelectWindow();
            pizzaWin.ShowDialog();
            textboxSum.Text = Convert.ToString(LocalParameters.sumValue) + " PLN";
        }
        private void buttonDish_Click(object sender, RoutedEventArgs e)
        {
            DishSelectWindow dishWindow = new DishSelectWindow();
            dishWindow.ShowDialog();
            textboxSum.Text = Convert.ToString(LocalParameters.sumValue) + " PLN";
        }
        private void buttonSoup_Click(object sender, RoutedEventArgs e)
        {
            SoupSelectWindow soupWindow = new SoupSelectWindow();
            soupWindow.ShowDialog();
            textboxSum.Text = Convert.ToString(LocalParameters.sumValue) + " PLN";
        }
        private void buttonDrink_Click(object sender, RoutedEventArgs e)
        {
            DrinkSelectWindow drinkWindow = new DrinkSelectWindow();
            drinkWindow.ShowDialog();
            textboxSum.Text = Convert.ToString(LocalParameters.sumValue) + " PLN";
        }
        private void buttonPlaceOrder_Click(object sender, RoutedEventArgs e)
        {
            if (LocalParameters.sumValue == 0)
            {
                MessageBox.Show("You cant order empty receipt!!");
            }
            else
            {
                OrderInformationWindow orderInfoWindow = new OrderInformationWindow();
                orderInfoWindow.ShowDialog();
                textboxSum.Text = Convert.ToString(LocalParameters.sumValue) + " PLN";
            }
        }
        private void buttonOrderHistory_Click(object sender, RoutedEventArgs e)
        {
            OrderHistoryWindow orderHisWindow = new OrderHistoryWindow();
            orderHisWindow.ShowDialog();
            textboxSum.Text = Convert.ToString(LocalParameters.sumValue) + " PLN";
        }
        private void buttonClose_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }
        private void buttonDeleteItem_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                var selector = orderGrid.SelectedItem as DataRowView;
                decimal value = (decimal)selector.Row["Value"];
                MenuParameters.orderTable.Rows.RemoveAt(orderGrid.SelectedIndex);
                LocalParameters.sumValue -= (decimal)value;
                textboxSum.Text = Convert.ToString(LocalParameters.sumValue);
                LogWriter.LogWrite("Successfully deleted item from receipt.");
            }
            catch (Exception itemerror)
            {
                MessageBox.Show("Error while trying to remove item");
                LogWriter.LogWrite(itemerror.ToString());
            }
        }
    }
}
