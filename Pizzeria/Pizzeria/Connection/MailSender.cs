﻿using System;
using System.Windows;
using System.Net;
using System.Net.Mail;
using Pizzeria.Logs;
using Pizzeria.Configuration;

namespace Pizzeria.Connection
{
    public static class MailSender
    {
        public static void MailSend()
        {
            string senderAddress = "lubieplacki3123@gmail.com";
            MailMessage msg = new MailMessage(senderAddress, LocalParameters.emailAddress);
            msg.Subject = $"Your order was placed in Pizzeria MetaPack at date and time: {DateTime.Now.ToShortDateString()} - {DateTime.Now.ToShortTimeString()}";
            msg.Body = $"Congratulations! Your order was placed at {DateTime.Now.ToShortDateString()} on : {DateTime.Now.ToShortTimeString()}!" +
                $"\nOrder Address: {LocalParameters.postAddress}\nValue of order {LocalParameters.sumValue} PLN.\nPrepare money, our man is on the way!!";
            SmtpClient client = new SmtpClient("smtp.gmail.com",587);
            client.EnableSsl = true;
            client.DeliveryMethod = SmtpDeliveryMethod.Network;
            client.UseDefaultCredentials = false;
            client.Credentials = new NetworkCredential(senderAddress, "rabarbar");
            try
            {
                client.Send(msg);
                MessageBox.Show("Order placed!");
                LogWriter.LogWrite($"Order placed.Email sent at: {LocalParameters.emailAddress}");
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message);
                LogWriter.LogWrite(e.ToString());
            }

        }
    }
}
