﻿using System;
using System.Windows;
using System.Data.SqlClient;
using Pizzeria.Logs;
using Pizzeria.Configuration;

namespace Pizzeria.Connection
{
    public class LocalSQLConnection
    {
        public void InitializeDBConnection()
        {
            try
            {
                using (var connection = new SqlConnection(LocalParameters.localSqlPath))
                using (SqlDataAdapter sqladapter = new SqlDataAdapter(LocalParameters.selectQuery, connection))
                {
                    connection.Open();
                    sqladapter.SelectCommand.CommandType = System.Data.CommandType.StoredProcedure;
                    sqladapter.Fill(MenuParameters.orderHistoryTable);
                    LogWriter.LogWrite("Loaded Database!!");
                }
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message);
                LogWriter.LogWrite(e.ToString());
            }
        }
        public void InsertDBHistoryRecord()
        {
            try
            {
                using (var connection = new SqlConnection(LocalParameters.localSqlPath))
                using (SqlCommand cmd = new SqlCommand(LocalParameters.addQuery, connection))
                {
                    connection.Open();
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@Date", DateTime.Now.ToShortDateString() + DateTime.Now.ToShortTimeString());
                    cmd.Parameters.AddWithValue("@LocalAddress", LocalParameters.postAddress);
                    cmd.Parameters.AddWithValue("@EmailAddress", LocalParameters.emailAddress);
                    cmd.Parameters.AddWithValue("@OrderVal", LocalParameters.sumValue);
                    cmd.Parameters.AddWithValue("@Info", LocalParameters.addInformation);
                    cmd.ExecuteNonQuery();
                    LogWriter.LogWrite("Added new history order record.");
                }
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message);
                LogWriter.LogWrite(e.ToString());
            }
        }
    }
}
